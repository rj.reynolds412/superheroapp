package com.example.superheroapp.domain

import android.util.Log
import com.example.superheroapp.model.SuperRepo
import com.example.superheroapp.model.response.SearchResponse
import com.example.superheroapp.model.response.SuperDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SearchCharacterUseCase @Inject constructor(private val superRepo: SuperRepo) {
    suspend operator fun invoke(name: String): Result<SearchResponse> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response = superRepo.searchCharacters(name)
            val searchedCharacter : SearchResponse  = response
            Result.success(searchedCharacter)
        } catch (ex: Exception) {
            Result.failure(ex)
        }
    }
}