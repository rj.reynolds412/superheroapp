package com.example.superheroapp.domain

import com.example.superheroapp.model.SuperRepo
import com.example.superheroapp.model.response.SearchResponse
import com.example.superheroapp.model.response.SuperDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetCharacterUseCase @Inject constructor(private val superRepo: SuperRepo) {
    suspend operator fun invoke(id: String): Result<SuperDTO> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response = superRepo.getCharacters(id)
            val characters: SuperDTO = response
            Result.success(characters)
        } catch (ex: Exception) {
            Result.failure(ex)
        }
    }
}
