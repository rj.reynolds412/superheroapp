package com.example.superheroapp.model.response


import com.google.gson.annotations.SerializedName

data class SearchResponse(
    val response: String = "",
    val results: List<SuperDTO> = emptyList(),
    @SerializedName("results-for")
    val resultsFor: String = ""
)