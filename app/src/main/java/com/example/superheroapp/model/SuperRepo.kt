package com.example.superheroapp.model

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SuperRepo @Inject constructor(private val superService: SuperService) {

    suspend fun getCharacters(id: String) = withContext(Dispatchers.IO) {
        superService.getCharacters(id)
    }


    suspend fun searchCharacters(name: String) = withContext(Dispatchers.IO) {
         superService.searchCharacters(name)
    }
}