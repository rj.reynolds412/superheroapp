package com.example.superheroapp.model.response


data class Image(
    val url: String = "",
)