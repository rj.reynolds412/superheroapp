package com.example.superheroapp.model.response


data class Work(
    val base: String = "",
    val occupation: String = "",
)