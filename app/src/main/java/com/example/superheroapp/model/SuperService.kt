package com.example.superheroapp.model

import com.example.superheroapp.model.response.SearchResponse
import com.example.superheroapp.model.response.SuperDTO
import retrofit2.http.GET
import retrofit2.http.Path

interface SuperService {

    companion object {

        const val BASE_URL = "https://superheroapi.com/api/10229790348560417/"
        private const val CHARACTER_ENDPOINT = "{id}"
        private const val SEARCH_ENDPOINT = "search/{name}"

    }

    @GET(CHARACTER_ENDPOINT)
    suspend fun getCharacters(@Path("id") id: String): SuperDTO

    @GET(SEARCH_ENDPOINT)
    suspend fun searchCharacters(@Path("name") name: String): SearchResponse
}