package com.example.superheroapp.model.response


data class SuperDTO(
    val appearance: Appearance = Appearance(),
    val biography: Biography = Biography(),
    val connections: Connections = Connections(),
    val id: String = "",
    val image: Image = Image(),
    val name: String = "",
    val powerstats: Powerstats = Powerstats(),
    val response: String = "",
    val work: Work = Work(),
)