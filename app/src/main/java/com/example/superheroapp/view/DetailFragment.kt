package com.example.superheroapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.superheroapp.databinding.FragmentDetailBinding
import com.example.superheroapp.model.response.SuperDTO
import com.example.superheroapp.viewModel.CharacterViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class DetailFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val characterViewModel by viewModels<CharacterViewModel>()
    private val args by navArgs<DetailFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentDetailBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    private fun initObservers() {
        lifecycleScope.launch {
            val details = args.id
            characterViewModel.getCharacters(details)
            characterViewModel.character.collectLatest { state ->
                val detail: SuperDTO = state.characters
                displayDetails(detail)
            }
        }
    }

    private fun displayDetails(details: SuperDTO) = with(binding) {
        details.run {
            ivDetail.load(image.url)
            tvDetailName.text = name
            val powerStats = listOfNotNull(
                "Power: ${powerstats.power}",
                "Combat: ${powerstats.combat}",
                "Durability: ${powerstats.durability}",
                "Speed: ${powerstats.speed}",
                "Intelligence: ${powerstats.intelligence}",
                "Strength: ${powerstats.strength}"
            )
            tvPowerstats.text = powerStats.joinToString("\n\n")

            val bio = mutableListOf(
                "Full Name: ${biography.fullName}",
                "Alter Egos: ${biography.alterEgos}",
            )
            if (biography.aliases.isNotEmpty()) {
                bio.add("Aliases: ${biography.aliases.joinToString("\n")}")
            }
            if (biography.placeOfBirth.isNotEmpty()) {
                bio.add("P.O.B: ${biography.placeOfBirth}")
            }
            bio.addAll(listOf(
                "First Appearance: ${biography.firstAppearance}",
                "Publisher: ${biography.publisher}",
                "Alignment: ${biography.alignment}"))
            tvBio.text = bio.joinToString("\n\n")

            val appearance = listOfNotNull(
                "Gender: ${appearance.gender}",
                "Race: ${appearance.race}",
                "Height: ${appearance.height.joinToString("\n")}",
                "Weight: ${appearance.weight.joinToString("\n")}",
                "Eye Color: ${appearance.eyeColor}",
                "Hair Color: ${appearance.hairColor}",
            )
            tvAppearance.text = appearance.joinToString("\n\n")

            val work = listOfNotNull(
                "Occupation: ${work.occupation}",
                "Base: ${work.base}"
            )
            tvWork.text = work.joinToString("\n\n")

            val connections = listOfNotNull(
                "Group Affiliation:\n${connections.groupAffiliation.replace(",", ",\n")}",
                "Relatives:\n${connections.relatives.replace(",", ",\n")}"
            )
            tvConnections.text = connections.joinToString("\n\n")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
