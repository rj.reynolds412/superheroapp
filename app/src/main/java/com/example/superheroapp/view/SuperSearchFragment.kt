package com.example.superheroapp.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.example.superheroapp.adapter.SuperAdapter
import com.example.superheroapp.databinding.FragmentSuperSearchBinding
import com.example.superheroapp.utils.hideKeyboard
import com.example.superheroapp.viewModel.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SuperSearchFragment : Fragment() {

    private var _binding: FragmentSuperSearchBinding? = null
    private val binding get() = _binding!!


    private val queryTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            if (query != null) {
                findNavController()
                    .navigate(SuperSearchFragmentDirections
                        .actionSuperSearchFragmentToHeroFragment(name = query))
            } else {
                hideKeyboard()
            }
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            binding.progressCircular.isVisible
            return true
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentSuperSearchBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()

    }

    private fun initViews() = with(binding) {
       svSearch.setOnQueryTextListener(queryTextListener)
    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}