package com.example.superheroapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.superheroapp.adapter.SuperAdapter
import com.example.superheroapp.databinding.FragmentHeroBinding
import com.example.superheroapp.model.response.SuperDTO
import com.example.superheroapp.viewModel.CharacterViewModel
import com.example.superheroapp.viewModel.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class HeroFragment : Fragment() {

    private var _binding: FragmentHeroBinding? = null
    private val binding: FragmentHeroBinding get() = _binding!!
    private val superAdapter by lazy { SuperAdapter(::superClick) }
    private val searchViewModel by viewModels<SearchViewModel>()
    private val args by navArgs<HeroFragmentArgs>()

     private fun superClick(character : SuperDTO){
        val detailNav = HeroFragmentDirections.actionHeroFragmentToDetailFragment(id = character.id)
        findNavController().navigate(detailNav)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentHeroBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        initViews()
    }



    private fun initViews() = with(binding) {
        val nameArgs = args.name
        rvCharacter.adapter = superAdapter
        searchViewModel.searchCharacters(nameArgs)
    }

    private fun initObservers() = lifecycleScope.launch {
        viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
            searchViewModel.search.collectLatest { state ->
                superAdapter.displayCharacters(state.searchResult)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}