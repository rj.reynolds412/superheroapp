package com.example.superheroapp.state

import com.example.superheroapp.model.response.SearchResponse

data class SearchState(
    val isLoading: Boolean = false,
    val searchResult: SearchResponse = SearchResponse(),
    val error: String? = null,
)
