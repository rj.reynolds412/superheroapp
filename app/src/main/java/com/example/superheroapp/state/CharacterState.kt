package com.example.superheroapp.state

import com.example.superheroapp.model.response.SearchResponse
import com.example.superheroapp.model.response.SuperDTO

data class CharacterState(
    val isLoading: Boolean = false,
    val characters: SuperDTO = SuperDTO(),
    val error: String? = null,
)
