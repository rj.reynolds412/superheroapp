package com.example.superheroapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.superheroapp.databinding.ItemCharacterBinding
import com.example.superheroapp.model.response.Image
import com.example.superheroapp.model.response.SearchResponse
import com.example.superheroapp.model.response.SuperDTO

class SuperAdapter(val superClick: (character : SuperDTO) -> Unit) : RecyclerView.Adapter<SuperAdapter.SuperViewHolder>() {
    private var character = mutableListOf<SuperDTO>()

    class SuperViewHolder(private val binding: ItemCharacterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun getCharacters(character: SuperDTO) = with(binding) {
            ivCharacter.load(character.image.url)
            tvCharacterName.text = character.name

        }
        companion object {
            fun getInstance(parent: ViewGroup) = ItemCharacterBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { SuperViewHolder(it) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuperViewHolder {
       return SuperViewHolder.getInstance(parent).apply {
           itemView.setOnClickListener { superClick(character[adapterPosition]) }
       }
    }

    override fun onBindViewHolder(holder: SuperViewHolder, position: Int) {
        val character = character[position]
        holder.getCharacters(character)
    }

    override fun getItemCount(): Int = character.size

    fun displayCharacters(character: SearchResponse){
        this.character.run {
            val oldSize = size
            clear()
            notifyItemRangeRemoved(0, oldSize)
            addAll( character.results)
            notifyItemRangeInserted(0, size)
        }
    }
}