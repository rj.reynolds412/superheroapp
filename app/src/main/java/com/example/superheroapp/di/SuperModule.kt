package com.example.superheroapp.di

import com.example.superheroapp.domain.GetCharacterUseCase
import com.example.superheroapp.domain.SearchCharacterUseCase
import com.example.superheroapp.model.SuperRepo
import com.example.superheroapp.model.SuperService
import com.example.superheroapp.model.SuperService.Companion.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object SuperModule {

    @Provides
    @Singleton
    fun providesSuperService(): SuperService = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(SuperService::class.java)

    @Provides
    @Singleton
    fun providesSuperRepo(superService: SuperService)
            : SuperRepo = SuperRepo(superService)

    @Provides
    @Singleton
    fun providesCharacterUseCase(superRepo: SuperRepo): GetCharacterUseCase =
        GetCharacterUseCase(superRepo)

    @Provides
    @Singleton
    fun provideSearchCharacterUseCase(superRepo: SuperRepo): SearchCharacterUseCase =
        SearchCharacterUseCase(superRepo)
}
