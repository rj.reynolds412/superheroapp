package com.example.superheroapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.superheroapp.domain.GetCharacterUseCase
import com.example.superheroapp.model.response.SearchResponse
import com.example.superheroapp.model.response.SuperDTO
import com.example.superheroapp.state.CharacterState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CharacterViewModel @Inject constructor(private val getCharacterUseCase: GetCharacterUseCase) :
    ViewModel() {

    private val _character = MutableStateFlow(CharacterState(isLoading = true))
    val character = _character.asStateFlow()

    fun getCharacters(id: String) {
        viewModelScope.launch {
            val result = getCharacterUseCase.invoke(id)
            if (result.isSuccess) {
                val characters : SuperDTO = result.getOrThrow()
                _character.value = CharacterState(
                    isLoading = false,
                    characters = characters,
                )
            } else {
                _character.value = CharacterState(
                    isLoading = false,
                    error = result.exceptionOrNull()?.message
                )
            }
        }
    }
}