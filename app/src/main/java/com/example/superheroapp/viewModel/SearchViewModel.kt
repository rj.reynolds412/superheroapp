package com.example.superheroapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.superheroapp.domain.SearchCharacterUseCase
import com.example.superheroapp.model.response.SearchResponse
import com.example.superheroapp.state.SearchState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val searchCharacterUseCase: SearchCharacterUseCase,
) : ViewModel() {

    private val _search = MutableStateFlow(SearchState(isLoading = true))
    val search = _search.asStateFlow()

    fun searchCharacters(name: String) {
        viewModelScope.launch {
            val searchResult = searchCharacterUseCase.invoke(name)
            if (searchResult.isSuccess) {
                val searchedCharacter : SearchResponse = searchResult.getOrThrow()
                _search.value = SearchState(
                    isLoading = false,
                    searchResult = searchedCharacter
                )
            } else {
                _search.value = SearchState(
                    isLoading = false,
                    error = searchResult.exceptionOrNull()?.message
                )
            }
        }
    }
}